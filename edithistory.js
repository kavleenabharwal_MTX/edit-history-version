function getChanges(previousVersion, currentVersion) {
    let diff = _.reduce(previousVersion, function(result, value, key) {
        if (key !== 'Modified_time' && key !== 'Modified_By' && key !== 'id') {
            if (_.isArray(value)) {
                let completeAddressVersionObjectArray = [];
                if (previousVersion[key]) {
                    _.forEach(previousVersion[key], function(previousObjectValue, index) {
                        if (currentVersion[key] && currentVersion[key][index]) {
                            let addressVersionObjectArray = getChanges(previousObjectValue, currentVersion[key][index]);
                            _.forEach(addressVersionObjectArray, function(modifiedValue) {
                                completeAddressVersionObjectArray.push(key + '.' + index + '.' + modifiedValue);
                            });
                        } else {
                            let innerObject = _.reduce(previousObjectValue, function(result, value, key) {
                                return result.concat(key);
                            }, []);
                            _.forEach(innerObject, function(modifiedValue) {
                                completeAddressVersionObjectArray.push(key + '.' + index + '.' + modifiedValue);
                            });
                        }
                    });
                }
                return result.concat(completeAddressVersionObjectArray);
            } else if (_.isObject(value)) {
                if(key ==='MonitoringCompletionDate' || key==='EndOfMonitoringDate' || key ==='DateDeparture'){
                    if (currentVersion) {
                        let previousValue = value ? value : '';
                        let currentValue = currentVersion[key] ? currentVersion[key] : '';
                        if(checkIfValueIsNotEmpty(previousValue._seconds) && checkIfValueIsNotEmpty(currentValue._seconds)){
                         let returnValue = _.isEqual(previousValue._seconds, currentValue._seconds) ? result : result.concat(key);
                         return returnValue;

                        }
                        else{
                            let returnValue = _.isEqual(previousValue, currentValue) ? result : result.concat(key);
                            return returnValue;

                        }
                    } else {
                        return result.concat(key);
                    }
                } 
                return result             
            } else {
                if (currentVersion) {
                    let previousValue = value ? value : '';
                    let currentValue = currentVersion[key] ? currentVersion[key] : '';
                    let returnValue = _.isEqual(previousValue, currentValue) ? result : result.concat(key);
                    return returnValue;
                } else {
                    return result.concat(key);
                }
            }
        } else {
            return result;
        }
    }, []);
    return diff;
}



  getTravelerVersionsByMid: async(req, res, db) => {
        let changeHistory = [];
        let allVersions = [];
        let error;
        
        let  querySnapshot = await travelerVersionTable.findAll({ 
            where: { 
                'data.Monitoring_ID' : req.query.mid
            } 
            
        });
        
        for(let i=0; i < querySnapshot.length; i++){
            let doc= querySnapshot[i]
            const data = Object.assign({}, doc.dataValues.data, {
                id: doc.id
            });
            allVersions.push(data);

        }
        
            allVersions = allVersions.sort((a, b) => {
                let ftime = a.Modified_time;
                let ltime = b.Modified_time;

                var a1 = (isObject(ftime) ? firestoreToMoment(ftime) : moment(ftime));
                var b1 = (isObject(ltime) ? firestoreToMoment(ltime) : moment(ltime));
                return a1.isAfter(b1)?1:-1;
            });
        if (allVersions.length > 0) {
            let previousVersion;
            allVersions.forEach(currentVersion => {
                let versionObject = {};
                versionObject['Modified_By'] = currentVersion['Modified_By'];
                versionObject['Modified_Time'] = currentVersion['Modified_time'];
                versionObject['changes'] = [];
                versionObject['instanceData'] = currentVersion;
                if (previousVersion) {
                    let versionObjectArray = [];
                    versionObjectArray = getChanges(previousVersion, currentVersion);
                    versionObjectArray = versionObjectArray.concat(getChanges(currentVersion, previousVersion));
                    let uniqueValues = _.uniq(versionObjectArray);
                    versionObject['changes'] = uniqueValues;
                    changeHistory.push(versionObject);
                } else {
                    changeHistory.push(versionObject);
                }
                previousVersion = currentVersion;
            });
        }
        res.send({
            data: changeHistory,
            error: error
        });
    }